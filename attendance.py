from configparser import ConfigParser
import bs4
import mechanize
from http import cookiejar
from dialog import PasswordDialog


def validate_login(response, reference_text):
    soup = bs4.BeautifulSoup(response, "html.parser")
    test_item = soup.find("li", text = reference_text, attrs={"class": "breadcrumb-item"})
    if not test_item:
        print("Something went wrong. Login failed.")
        return False
    else:
        print("Login successful!")
        return True


def recieve_login_data(user, passwd, url):
    config['DEFAULT']['username'] = user
    config['DEFAULT']['password'] = passwd
    config['DEFAULT']['url'] = url
    with open("config.ini", "w") as file:
        config.write(file)


def get_attendance_link(response):
    text = "Anwesenheit erfassen"
    soup = bs4.BeautifulSoup(response, "html.parser")
    
    link = soup.find("a", href=True, text=text)
    if not link:
        return None
    else:
        return link['href']


config = ConfigParser()
with open("config.ini", "r") as file:
    config.read_file(file)
username: str = None
password: str = None
url: str = None

try:
    url = config['DEFAULT']['url']
    username = config['DEFAULT']['username']
    password = config['DEFAULT']['password']

except Exception as e:
    print("Either could not load the config file,or parameters are missing.")


if not username or not password or not url:
    PasswordDialog(callback=recieve_login_data)

print(f"URL: {config['DEFAULT']['url']}")
print(f"username: {config['DEFAULT']['username']}")
print(f"password: {'*' * len(config['DEFAULT']['password'])}")

#cj = cookiejar.CookieJar()
cj = cookiejar.LWPCookieJar()
br = mechanize.Browser()
br.addheaders = [( 'User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1' )]

br.set_handle_equiv(True)
br.set_handle_gzip(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)

# Follows refresh 0 but not hangs on refresh > 0
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)


br.set_cookiejar(cj)
br.open(url)

br.select_form(nr=0)
br.form['username'] = config["DEFAULT"]["username"]
br.form['password'] =  config["DEFAULT"]["password"]
response = br.submit(id="loginbtn")
#response = br.open(url)
response_str = response.read()


# validate that login was successful checking for known text on page
if not validate_login(response_str, "Bericht zu Anwesenheit"):
    quit()
link = get_attendance_link(response_str)
if not link:
    print("No attendance link was found.")
    quit()


response = br.open(link)

br.select_form(nr=1)
control_name = br.form.controls[5].get_items()[0]

br.find_control("status").items[0].selected=True
print(f"Control element {control_name} is set to {br.find_control('status').items[0].selected}")
resp = br.submit(id="id_submitbutton")
resp_str = resp.read()

response = br.open(url)

if not get_attendance_link(response.read()):
    print("Attendance successfully set.")
else:
    print("Something went wrong. Please check attendance yourself.")


import tkinter as tk
root = tk.Tk()

class PasswordDialog:

    callback: callable = None
    #root = tk.Tk()
    username_input:tk.Entry
    password_input:tk.Entry
    url_input:tk.Entry

    def on_button_pressed(self):
        root.quit()
        self.callback(self.username_input.get(), self.password_input.get(), self.url_input.get())
        
    def __init__(self, callback: callable):
        self.callback = callback

        root.geometry("255x165")
        root.title("Login")
        root.resizable(False,False)
        tk.Label(root, text="Please enter your Moodle attendance url.").pack()
        self.url_input = tk.Entry(root)
        self.url_input.pack()
        tk.Label(root, text="Please enter your Moodle login.").pack()
        tk.Label(root, text="Username:").pack()
        self.username_input = tk.Entry(root)
        self.username_input.pack()
        tk.Label(root, text="Password:").pack()
        self.password_input = tk.Entry(root, show = '*')
        self.password_input.pack()
        ok_btn = tk.Button(root, text="OK", command=self.on_button_pressed)
        ok_btn.pack()
        root.mainloop()

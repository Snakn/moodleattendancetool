# HOW TO USE

This project uses virtualenv to install the requirements in.

It is important that the python command is known by your operating system if it is not found it may use the 'python3' command

## Automatic setup 

* Execute setup.bat script on Windows
* Execute run.bat



## Manual setup

* Setup virtualenv

  ```pip install virtualenv
  #if this does not work try
  python -m pip install virtualenv
  #or
  python3 -m pip install virtualenv
  virtualenv venv
  ```

* Setup requirements

  ``````
  #on windows
  call venv\Scripts\activate.bat
  
  #Linux/Mac
  source venv\bin\activate
  
  pip install -r requirements.txt
  ``````

* Run the program

  ```
  #on windows
  call venv\Scripts\activate.bat
  
  #Linux/Mac
  source venv\bin\activate
  
  python attendance.py
  ```

  Enter the url to your moodle attendance page
  ![grafik01.png](./grafik01.png)
  
  alternatively edit the config.ini in a text editor
  ![grafik.png](./grafik.png)


